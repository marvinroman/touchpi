#!/usr/bin/python3

from gpiozero.pins.pigpio import PiGPIOFactory
from gpiozero import Device, DistanceSensor
from subprocess import call
from time import sleep

Device.pin_factory = PiGPIOFactory()
interval=1

sensor = DistanceSensor(echo=24, trigger=23)
while True:
    distance = sensor.distance * 100
    if distance > 2 and distance < 60:
        call(["xset", "dpms", "force", "on"])
    sleep(interval)