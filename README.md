# Setup Pi

## Install Software libraries
sudo apt install python3-pip pigpio pigpiod git
pip3 install gpiozero

## Start Necessary Daemons
```bash
sudo systemctl enable pigpiod
sudo systemctl start pigpiod
```

## Clone Repository
git clone [] $HOME/app

## Setup Power Management
`sudo nano /etc/xdg/openbox/autostart`  
```bash
xset s blank
xset s 300

/home/pi/app/send-temperature.py &
DISPLAY=:0 /home/pi/app/distance-wakeup.py &

chromium-browser  --noerrdialogs --disable-infobars --kiosk $KIOSK_URL
```
## Setup Environment Variables
`sudo nano /etc/xdg/openbox/environment`  
```bash
export KIOSK_URL=http://homeassistant.local:8123
```