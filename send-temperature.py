#!/usr/bin/python3

import RPi.GPIO as GPIO
import os
from dht11 import DHT11
from time import sleep
from paho.mqtt import client as mqtt_client

# initialize GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()

# Setup MQTT
broker = 'homeassistant.local'
port = 1883
temp_topic = "climate/temperature/status"
hum_topic = "climate/humidity/status"
client_id = 'mqtt.touchpi.local'
username = 'mosquitto'
password = 'qPrfj5obtfyxSKBeGLAm'
interval = 30

while (True):
    response = os.system("ping -c 1 " + broker)
    if response == 0:
        break
    else:
        sleep(5)


# Continually loop through querying temperature and sending to MQTT topics
def loop(client: mqtt_client):
    while(True):
        # read data using pin GPIO18
        instance = DHT11(pin = 18)
        result = instance.read()
        if result.is_valid():
            temerature = (result.temperature * 1.8 + 32) * 0.9352
            # Push temperature to the temperature topic
            publish(client, temp_topic, temerature)
            # Push humidity to the humidity topic
            publish(client, hum_topic, result.humidity)
            # Pause before looping again
            sleep(interval)


# Publish messages to MQTT
def publish(client: mqtt_client, topic, msg):
    result = client.publish(topic, payload=str(msg), qos=2, retain=True)
    status = result[0]
    if status != 0:
        print(f"Failed to send message to topic {topic}")


# Initiate connection to the MQTT broker
def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc != 0:
            print("Failed to connect, return code %d\n", rc)
    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


if __name__ == '__main__':
    client = connect_mqtt()
    client.loop_start()
    loop(client)